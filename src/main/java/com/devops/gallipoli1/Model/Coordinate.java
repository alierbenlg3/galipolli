package com.devops.gallipoli1.Model;

public class Coordinate {

    private static String _ligne;
    private static String _colonne;
    private static Coordinates _coordinates;

    public static void setLigne(String ligne) {
        _ligne = ligne;
    }

    public static String getLigne() {
        return _ligne;
    }

    public static void setColonne(String colonne) {
        _colonne = colonne;
    }

    public static String getColonne() {
        return _colonne;
    }

    public static void setCoordinates(Coordinates coordinates) {
        _coordinates = coordinates;
    }

    public static int[] getCoordinates() {
        return new int[]{_coordinates.getRow(), _coordinates.getCol()};
    }
}
