package com.devops.gallipoli1.Model;

import javafx.scene.layout.Pane;

public class Grid {
    private Object[][] gridData;

    public Grid(int rows, int columns) {
        gridData = new Object[rows][columns];
    }

    public int getRowCount() {
        return gridData.length;
    }

    public int getColumnCount() {
        return gridData[0].length;
    }

    public Object getObjectAt(int row, int col) {
        return gridData[row][col];
    }

    public void setObjectAt(int row, int col, int width, Pane bateau) {
        for (int i = 0; i < width; i++) {
            if (col + i >= getColumnCount()) {
                // Vérifier si l'objet dépasse les limites de la grille
                throw new IllegalArgumentException("L'objet dépasse les limites de la grille.");
            }
            if (gridData[row][col + i] != null) {
                // Vérifier si l'emplacement est déjà occupé
                throw new IllegalArgumentException("L'emplacement est déjà occupé.");
            }
            gridData[row][col + i] = bateau;
            int coli = col +i;
            bateau.setStyle("-fx-background-color: red; -fx-border-width: 2px; -fx-border-color: black;");
            System.out.println(bateau.getStyle() + " -- Ligne: " + row + " -- Colonne: " + coli);
        }
    }
}
