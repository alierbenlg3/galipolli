package com.devops.gallipoli1.Model;

import java.io.Serializable;

public class Coordinates implements Serializable {
    private int row;
    private int col;

    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}