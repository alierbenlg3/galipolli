package com.devops.gallipoli1;

import com.devops.gallipoli1.Model.Coordinate;
import com.devops.gallipoli1.client.ClientThread;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Client{

    private static final Lock lock = new ReentrantLock();

        public static synchronized void sendTir(){
        try {

            PrintWriter writer = new PrintWriter(ClientThread.getSocket().getOutputStream(), true);

            int[] coordinates = Coordinate.getCoordinates();
            int row = coordinates[0];
            int col = coordinates[1];

            writer.println(ClientThread.getSocket().getLocalPort());
            writer.println("Coordonnées[" + row + "]-" + "[" + col + "]");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Platform.startup(() -> {
            Stage stage = new Stage();
            ClientThread clientThread = new ClientThread(stage);
            clientThread.start();
        });
    }
}

