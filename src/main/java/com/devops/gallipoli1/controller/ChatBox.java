package com.devops.gallipoli1.controller;

import com.devops.gallipoli1.client.ClientThread;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatBox {

    public static void verifierCoordonnees() throws IOException {
        Object obj = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(ClientThread.getSocket().getInputStream()));
        try {
                obj = reader.readLine();
                Object finalObj = obj;

            TextArea myTextArea = (TextArea) ClientThread.getPrimaryStage().getScene().lookup("#myTextArea");
            myTextArea.appendText("\nSender: " + finalObj + "\n");
                if(obj.toString().startsWith("Coordonnées")){
                    String str = obj.toString();
                    Pattern pattern = Pattern.compile("\\[(.*?)\\]");
                    Matcher matcher = pattern.matcher(str);

                    int ligne = 0;
                    int colonne = 0;
                    int count = 0;

                    while (matcher.find()) {
                        String extractedText = matcher.group(1);
                        int number = Integer.parseInt(extractedText);

                        if (count == 0) {
                            ligne = number;
                        } else if (count == 1) {
                            colonne = number;
                        }
                        count++;
                    }
                    myTextArea.appendText("\n" + "Ligne: " + ligne + ", Colonne: " + colonne + ", Contenu: " + GrilleController1.getGrid1().getObjectAt(ligne, colonne) + "\n");

                    PrintWriter writer = new PrintWriter(ClientThread.getSocket().getOutputStream(), true);
                    if(GrilleController1.getGrid1().getObjectAt(ligne, colonne) != null){
                        writer.println("Touché");
                        String line = reader.readLine();
                        myTextArea.setText(line);
                    }
                    else{
                        writer.println("Raté");
                        String line = reader.readLine();
                        myTextArea.setText(line);
                    }
                }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
