package com.devops.gallipoli1.controller;

import com.devops.gallipoli1.Client;
import com.devops.gallipoli1.client.ClientThread;
import com.devops.gallipoli1.Model.Coordinates;
import com.devops.gallipoli1.Model.Coordinate;
import com.devops.gallipoli1.Model.Grid;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.net.Socket;
import java.util.Random;

public class GrilleController1{

    static Socket socket = ClientThread.getSocket();

    @FXML
    private Button idButton00;
    @FXML
    private Button idButton01;
    @FXML
    private TextField ligne;
    @FXML
    private TextField colonne;
    @FXML
    private Label clientPortLabel;

    private Random random1 = new Random();
    private static Grid grid1 = new Grid(5, 5);

    public void initialize() {

        int clientPort = socket.getLocalPort();
        clientPortLabel.setText("Client Port: " + clientPort);

        idButton00.setOnAction(this::ajouterBateau);
        idButton01.setOnAction(this::envoyerTir);
    }

    private void ajouterBateau(ActionEvent event) {

        Text cuirasseText = new Text("Cuirassé");
        Pane cuirrase = new Pane();
        cuirrase.setPrefSize(200, 200);
        cuirrase.getChildren().add(cuirasseText);

        Text corvetteText = new Text("Corvette");
        Pane corvette = new Pane();
        corvette.setPrefSize(200,200);
        corvette.getChildren().add(corvetteText);

        addObjectToGrid1(3, cuirrase);
        addObjectToGrid1(2, corvette);
    }

    public void updateLigne() {
        Coordinate.setLigne(ligne.getText());
    }

    public void updateColonne() {
        Coordinate.setColonne(colonne.getText());
    }

    public synchronized void envoyerTir(ActionEvent event){

        updateLigne();
        updateColonne();

        int line = Integer.parseInt(Coordinate.getLigne());
        int column = Integer.parseInt(Coordinate.getColonne());

        Coordinates coordinates = new Coordinates(line, column);
        Coordinate.setCoordinates(coordinates);
        Client.sendTir();
    }

    public void addObjectToGrid1(int width, Pane bateau) {
        int row;
        int col;

        // Générer des indices de ligne et de colonne aléatoires qui conviennent à l'objet
        do {
            row = random1.nextInt(grid1.getRowCount());
            col = random1.nextInt(grid1.getColumnCount() - width + 1);
        } while (!isValidPosition1(row, col, width));

        grid1.setObjectAt(row, col, width, bateau);
    }

    public boolean isValidPosition1(int row, int col, int width) {
        // Vérifier si l'emplacement et la largeur de l'objet sont valides
        for (int i = 0; i < width; i++) {
            if (col + i >= grid1.getColumnCount() || grid1.getObjectAt(row, col + i) != null) {
                return false;
            }
        }
        return true;
    }

    public static Grid getGrid1() {
        return grid1;
    }
}