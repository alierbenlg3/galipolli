package com.devops.gallipoli1.useless;

import java.io.Serializable;

public class GridUpdate implements Serializable {
    private int row;
    private int col;
    private int width;
    private Object obj;

    public GridUpdate(int row, int col, int width, Object obj) {
        this.row = row;
        this.col = col;
        this.width = width;
        this.obj = obj;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getWidth() {
        return width;
    }

    public Object getObj() {
        return obj;
    }
}


