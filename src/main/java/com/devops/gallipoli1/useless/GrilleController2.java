//package com.devops.gallipoli1.useless;
//
//import com.devops.gallipoli1.Client;
//import com.devops.gallipoli1.Model.ButtonInfo;
//import com.devops.gallipoli1.Model.Grid;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.scene.control.Button;
//import javafx.scene.layout.GridPane;
//
//import java.util.Arrays;
//import java.util.Random;
//
//public class GrilleController2 {
//
//    public Button idButton231;
//    public Button idButton241;
//    public Button idButton331;
//    public Button idButton341;
//    public Button idButton431;
//    public Button idButton441;
//    public Button idButton001;
//    public Button idButton011;
//    public Button idButton021;
//    public Button idButton031;
//    public Button idButton041;
//    public Button idButton141;
//    public Button idButton131;
//    public Button idButton101;
//    public Button idButton111;
//    public Button idButton121;
//    public Button idButton201;
//    public Button idButton211;
//    public Button idButton221;
//    public Button idButton301;
//    public Button idButton311;
//    public Button idButton321;
//    public Button idButton401;
//    public Button idButton411;
//    public Button idButton421;
//
//    private Random random2 = new Random();
//    private Grid grid2 = new Grid(5, 5);
//
//    public void initialize() {
//
//        addObjectToGrid2(3, "Cuirassé");
//        addObjectToGrid2(4, "Croiseur");
//        addObjectToGrid2(2, "Corvette");
//
//        for (Button button : Arrays.asList(idButton001,idButton011,idButton021,idButton031,idButton041,idButton101,idButton111,idButton121,idButton131,idButton141,idButton201,idButton211,idButton221,idButton231,idButton241,idButton301,idButton311,idButton321,idButton331,idButton341,idButton441,idButton431,idButton421,idButton411,idButton401)) {
//            button.setOnAction(this::handleButtonClick1);
//        }
//    }
//
//    @FXML
//    private void handleButtonClick1(ActionEvent event){
//        Button clickedButton = (Button) event.getSource();
//        int row = GridPane.getRowIndex(clickedButton);
//        int col = GridPane.getColumnIndex(clickedButton);
//
//        String idbuton = clickedButton.getId();
//
//        Object bateau = grid2.getObjectAt(row, col);
//        String style;
//        if (bateau != null) {
//            System.out.println("Objet trouvé : " + bateau + " ID Button " + idbuton);
//            style = "-fx-background-color: #3f1b1b; -fx-border-color: #000003; -fx-border-width: 2;";
//            clickedButton.setStyle(style);
//        } else {
//            System.out.println("Aucun objet trouvé à cet emplacement.");
//            style = "-fx-background-color: #46468c;";
//            clickedButton.setStyle(style);
//        }
//
//        ButtonInfo buttonInfo = new ButtonInfo(row, col, bateau);
//        Client.sendMessageToServer((buttonInfo.getCol()), (buttonInfo.getRow()), buttonInfo.getBateau());
//    }
//
//    private void addObjectToGrid2(int width, Object obj) {
//        int row;
//        int col;
//
//        // Générer des indices de ligne et de colonne aléatoires qui conviennent à l'objet
//        do {
//            row = random2.nextInt(grid2.getRowCount());
//            col = random2.nextInt(grid2.getColumnCount() - width + 1);
//        } while (!isValidPosition2(row, col, width));
//
//        grid2.setObjectAt2(row, col, width, obj);
//    }
//
//    private boolean isValidPosition2(int row, int col, int width) {
//        // Vérifier si l'emplacement et la largeur de l'objet sont valides
//        for (int i = 0; i < width; i++) {
//            if (col + i >= grid2.getColumnCount() || grid2.getObjectAt(row, col + i) != null) {
//                return false;
//            }
//        }
//        return true;
//    }
//}
