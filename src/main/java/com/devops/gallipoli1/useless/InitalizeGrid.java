//package com.devops.gallipoli1.useless;
//
//import com.devops.gallipoli1.Model.Grid;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.ObjectOutputStream;
//import java.io.Serializable;
//import java.nio.charset.StandardCharsets;
//import java.util.Random;
//
//public class InitalizeGrid implements Serializable {
//
//    private Random random1 = new Random();
//    private Grid grid = new Grid(5, 5);
//
//
//
//    private void addObjectToGrid(int width, Object obj) {
//        int row;
//        int col;
//
//        // Générer des indices de ligne et de colonne aléatoires qui conviennent à l'objet
//        do {
//            row = random1.nextInt(grid.getRowCount());
//            col = random1.nextInt(grid.getColumnCount() - width + 1);
//        } while (!isValidPosition(row, col, width));
//
//        //grid.setObjectAt(row, col, width, obj);
//
////        GridUpdate update = new GridUpdate(row, col, width, obj);
////        ByteArrayOutputStream bos = new ByteArrayOutputStream();
////        try {
////            ObjectOutputStream oos = new ObjectOutputStream(bos);
////            oos.writeObject(update);
////            byte[] bytes = bos.toByteArray();
////            clientHandler.writeToClient(new String(bytes, StandardCharsets.UTF_8));
////        } catch (IOException e) {
////            e.printStackTrace();
////            // Gérer l'exception ici
////        }
//    }
//
//    private boolean isValidPosition(int row, int col, int width) {
//        // Vérifier si l'emplacement et la largeur de l'objet sont valides
//        for (int i = 0; i < width; i++) {
//            if (col + i >= grid.getColumnCount() || grid.getObjectAt(row, col + i) != null) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    public void initializeGridWithShips() {
//        addObjectToGrid(3, "Cuirassé");
//        addObjectToGrid(4, "Croiseur");
//        addObjectToGrid(2, "Corvette");
//    }
//}
