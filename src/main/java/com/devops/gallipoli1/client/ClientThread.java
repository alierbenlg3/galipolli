package com.devops.gallipoli1.client;

import com.devops.gallipoli1.controller.ChatBox;
import com.devops.gallipoli1.controller.GrilleController1;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.net.Socket;
import java.nio.file.Paths;

public class ClientThread extends Thread {
    private static Stage primaryStage;
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 8888;
    private static Socket _socket;

    public ClientThread(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void run() {
        try {
            _socket = new Socket(SERVER_IP, SERVER_PORT);

            FXMLLoader loader1 = new FXMLLoader(Paths.get("C:/Users/aerben/gallipoli1 sauvegarde 21-01_2024/src/main/java/com/devops/gallipoli1/view/Grille1.fxml").toUri().toURL());
            loader1.setController(new GrilleController1());
            Parent root1 = loader1.load();
            root1.setTranslateX(50);

            FXMLLoader loader2 = new FXMLLoader(Paths.get("C:/Users/aerben/gallipoli1 sauvegarde 21-01_2024/src/main/java/com/devops/gallipoli1/view/ChatBox.fxml").toUri().toURL());
            loader2.setController(new ChatBox());
            Parent root2 = loader2.load();
            root2.setTranslateX(100);

            Separator separator = new Separator();
            separator.setOrientation(Orientation.VERTICAL);

            HBox hbox = new HBox(root1, root2);

            Platform.runLater(() -> {
                primaryStage.setScene(new Scene(hbox, 1200, 550));
                primaryStage.setTitle("Votre application JavaFX");
                primaryStage.show();
            });

            Task<Void> task = new Task<>() {
                @Override
                protected Void call() throws Exception {
                    while (true) {
                        //GrilleController1.verifierCoordonnees();
                        ChatBox.verifierCoordonnees();
                    }
                }
            };
            new Thread(task).start();
            System.out.println(_socket.getLocalPort());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Socket getSocket(){
        return _socket;
    }

    public static Stage getPrimaryStage(){
        return primaryStage;
    }
}

