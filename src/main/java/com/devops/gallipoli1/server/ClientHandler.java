package com.devops.gallipoli1.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;



public class ClientHandler extends Thread {
    private final Socket clientSocket;
    public List<Socket> clients;

    public ClientHandler(Socket clientSocket, List<Socket> client) {
        this.clientSocket = clientSocket;
        this.clients = client;
    }

    @Override
    public void run() {

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        while (true) {
            Object obj = null;
            try {
                obj = reader.readLine();
                broadcastMessage(obj);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void broadcastMessage(Object message) { //prend en parametre objet message
        for (Socket clientSockets : clients) { //boucle qui itère tout les socket stocké dans clients
            if (clientSockets != clientSocket) {
                try {
                    PrintWriter writer = new PrintWriter(clientSockets.getOutputStream(), true);
                    writer.println(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}