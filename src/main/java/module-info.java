module com.devops.gallipoli1 {
    requires javafx.controls;
    requires javafx.fxml;



    opens com.devops.gallipoli1 to javafx.fxml;
    exports com.devops.gallipoli1;

    exports com.devops.gallipoli1.Model;
    exports com.devops.gallipoli1.useless;
    opens com.devops.gallipoli1.useless to javafx.fxml;
    opens com.devops.gallipoli1.Model to javafx.fxml;
    exports com.devops.gallipoli1.controller;
    opens com.devops.gallipoli1.controller to javafx.fxml;
    exports com.devops.gallipoli1.client;
    opens com.devops.gallipoli1.client to javafx.fxml;
    exports com.devops.gallipoli1.server;
    opens com.devops.gallipoli1.server to javafx.fxml;

}